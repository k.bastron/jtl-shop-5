Plugins
=======

.. |br| raw:: html

    <br />

.. toctree::
    :hidden:

    allgemein
    aufbau
    infoxml
    bootstrapping
    variablen
    hooks
    hook_list
    container
    cache
    cron_jobs
    mailing
    portlets
    fehlercodes
    sicherheit
    payment_plugins
    hinweise

.. include:: /shop_plugins/map.rst.inc
