Breaking Changes
================

.. |rarr| raw:: html

   &rArr;

.. |br| raw:: html

   <br />

JTL-Shop 5.x  |rarr| 5.2.x
--------------------------


- **Systemvoraussetzung auf PHP 8.1 angehoben**

    Für den Betrieb von JTL-Shop 5.2.x ist PHP 8.1 Voraussetzung.
    Ab Version 5.2.3 ist JTL-Shop zusätzlich für PHP 8.2 freigegeben.

- **jQuery Version auf 3.1 angehoben**

    Mit JTL-Shop 5.2.x erfolgte für das Javascript-Framework *jQuery* ein Update von Version 3.0 auf
    Version 3.1. |br|

- **CLI unterstützt jetzt auch die Generierung der Bilder**

    Neues CLI-Kommando: cache:images:create

- **Neuer Hook HOOK_ORDER_DOWNLOAD_FILE (402)**

- **Kategoriebaum wird bei deaktiviertem Cache nicht mehr in der Session gespeichert**

- **Redis 6 mit ACL Benutzername+Passwort wird jetzt unterstützt**

- **Systemcheck Erweiterung: Info, welche Caching-Methoden serverseitig verfügbar sind**

- **Die Versionierung wurde geändert**

    "Semantic Versioning" : für JTL-Shop, |br|
    "API-Versioning" : intern für den Abgleich mit JTL-Wawi**

    Mit JTL-Shop 5.x wird die Versionsnummerierung des Onlineshops auf das allgemein gültige Verfahren
    `SemVer <http://semver.org/>`_ umgestellt. |br|
    Für die Verbindung zur JTL-Wawi wird intern weiterhin die bisherige Versionierung als interne API-Version geführt.

- **Die Upgrade-Möglichkeit von JTL-Shop *kleiner* Version 4.02 auf Version 5.x wurde entfernt**

    Nutzer vorheriger Versionen (zum Beispiel Version und kleiner 3.0x) müssen auf JTL-Shop Version 4.06
    aktualisieren, um von dort auf JTL-Shop Version 5.x upgraden zu können.

- **Das von JTL-Shop 4 bekannte Template "Evo" wird ab JTL-Shop 5.x als separates Projekt geführt
  und ist nicht mehr im Lieferumfang von JTL-Shop enhalten**

    Sie finden das Template "Evo" im JTL-Repository auf gitlab unter
    `Evo <https://gitlab.com/jtl-software/jtl-shop/templates>` und auf dem JTL Builds-Server unter
    `build.jtl-shop.de <https://build.jtl-shop.de/get/template_evo-5-0-0-rc-3.zip/template>`


- **Werkzeuge zum Kompilieren von Themes überarbeitet**

    In JTL-Shop 5.x werden Themes mit dem
    `JTL Theme Editor <https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_theme_editor>`_ übersetzt

    Weitere Informationen zur Verwendung dieser Plugins finden Sie im Abschnitt ":ref:`label_eigenestheme_kompilieren`".

- **Hooks erweitert/ergänzt/entfernt**

    Im Zuge der hier genannten Anpassungen und Änderungen haben sich auch verschiedene Hooks des Plugin-Systems
    geändert, wurden ergänzt oder sind ganz weggefallen. |br|
    Eine komplette Liste aller aktuell verfügbaren Hooks und ihrer Parameter finden Sie hier in der
    Entwicklerdokumentation unter ":doc:`/shop_plugins/hook_list`".

